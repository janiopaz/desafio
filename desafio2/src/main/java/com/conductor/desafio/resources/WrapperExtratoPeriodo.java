package com.conductor.desafio.resources;

import java.util.Date;

public class WrapperExtratoPeriodo {
	
	private Integer idConta;
	
	private Date data1;
	
	private Date data2;
	
	public WrapperExtratoPeriodo(){}
	
	public Date getData1() {return data1;}

	public void setData1(Date data1) {this.data1 = data1;}

	public Date getData2() {return data2;}

	public void setData2(Date data2) {this.data2 = data2;}

	public Integer getIdConta() {return idConta;}

	public void setIdConta(Integer idConta) {this.idConta = idConta;}
	
	

}
