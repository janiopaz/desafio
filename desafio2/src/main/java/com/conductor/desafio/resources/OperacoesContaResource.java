package com.conductor.desafio.resources;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.conductor.desafio.models.Conta;
import com.conductor.desafio.models.Transacao;
import com.conductor.desafio.repository.ContaRepository;
import com.conductor.desafio.repository.TransacaoRepository;

@RestController
@RequestMapping("/operacao")
public class OperacoesContaResource {
	
	@Autowired
	private  ContaRepository repositoryConta;
	@Autowired
	private  TransacaoRepository repositoryTransacao;			
	
	@PostMapping("/saldo")
	public ResponseEntity consultarSaldoDeUmaConta(@RequestBody @Valid WrapperTransacao wraper){
		
		Conta conta = repositoryConta.findOne(wraper.getIdConta());
	
		if(conta!=null){
		
			return ResponseEntity.status(HttpStatus.OK).body(conta.getSaldo());
		
		}else
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}
	
	@PostMapping("/bloqueioConta")
	public ResponseEntity bloquearConta(@RequestBody @Valid WrapperTransacao wraper){
		
		Conta conta = repositoryConta.findOne(wraper.getIdConta());
		
		if(conta!=null){
			
			conta.setFlagAtivo(false);
			repositoryConta.save(conta);
			
			return ResponseEntity.status(HttpStatus.OK).body(conta.isFlagAtivo());
		
		}else
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}
	
	
	@PostMapping("/depositar")
	public ResponseEntity depositar (@RequestBody @Valid WrapperTransacao wraper){

		Conta conta = repositoryConta.findOne(wraper.getIdConta());
		Date date = new Date();
		
		if((conta!=null) && (conta.isFlagAtivo())){
			
			Transacao transacao = new Transacao();
			transacao.setDataTransacao(date);
			transacao.setValor(wraper.getValor());
			transacao.setIdConta(conta);
			repositoryTransacao.save(transacao);
			
			conta.setSaldo(conta.getSaldo()+wraper.getValor());
			repositoryConta.save(conta);
			
			return ResponseEntity.status(HttpStatus.OK).body(conta.getSaldo());
		
		}else
		
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}
	@PostMapping("/saque")
	public ResponseEntity sacar (@RequestBody @Valid WrapperTransacao wraper){
		
		Conta conta = repositoryConta.findOne(wraper.getIdConta());
		Date date = new Date();
		
		if((conta!=null) && (conta.isFlagAtivo()) &&(wraper.getValor()<=conta.getLimiteSaqueDiario())){
			
			Transacao transacao = new Transacao();
			transacao.setDataTransacao(date);
			transacao.setValor(wraper.getValor());
			transacao.setIdConta(conta);
			repositoryTransacao.save(transacao);
			
			conta.setSaldo(conta.getSaldo()-wraper.getValor());
			repositoryConta.save(conta);
			
			return ResponseEntity.status(HttpStatus.OK).body(conta.getSaldo());
			
		}else if((wraper.getValor()>=conta.getLimiteSaqueDiario()) ||(!conta.isFlagAtivo())){
			
			return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
		
		}else
			System.out.println("alow");
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}
	
	@PostMapping("/extrato")
	public ResponseEntity<List<Transacao>> extrato(@RequestBody @Valid WrapperTransacao wraper){
		
		Conta conta = repositoryConta.findOne(wraper.getIdConta());
		List<Transacao> extratos = repositoryTransacao.findByidConta_id(wraper.getIdConta());
		
		if((conta != null) && (conta.isFlagAtivo()) && (extratos!= null)){
			
			return ResponseEntity.status(HttpStatus.OK).body(extratos);
		
		}else
		
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}
	
	@PostMapping("/extratoPorPeriodo")
	public ResponseEntity <List<Transacao>> extratoPorPeriodo(@RequestBody @Valid WrapperExtratoPeriodo wraper){
		
		Conta conta = repositoryConta.findOne(wraper.getIdConta());
		List<Transacao> extratos = repositoryTransacao.extratoPeriodo(wraper.getIdConta(),wraper.getData1(), wraper.getData2());
		
		if((conta != null) && (conta.isFlagAtivo()) && (extratos!= null)){
			
			return ResponseEntity.status(HttpStatus.OK).body(extratos);
		
		}else
		
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}
	
}
