package com.conductor.desafio.resources;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.conductor.desafio.models.Transacao;
import com.conductor.desafio.repository.TransacaoRepository;

@RestController
@RequestMapping("/transacao")
public class TransacaoResource {
	
	@Autowired
	private  TransacaoRepository repositoryTransacao;
	
	@GetMapping
	public @ResponseBody Iterable<Transacao> listarTransacoes(){
		
		Iterable<Transacao> listaTransacoes = repositoryTransacao.findAll();
		
		return listaTransacoes;
	}
	
	@GetMapping("/{id}")
	public 	ResponseEntity<Transacao> buscar(@PathVariable Integer id){
		
		Transacao transacao = repositoryTransacao.findOne(id);
		
		if(transacao == null){
		
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(transacao);
	}
	
	@PutMapping("/{id}")
	public 	ResponseEntity<Transacao> atualizar(@PathVariable Integer id,@RequestBody @Valid Transacao transacao){
		
		Transacao existente = repositoryTransacao.findOne(id);
		
		if(existente == null){
		
			return ResponseEntity.notFound().build();
		}
		
		BeanUtils.copyProperties(transacao, existente, "id");
		
		existente = repositoryTransacao.save(existente);
		
		return ResponseEntity.ok(existente);
	}
	
	@PostMapping()
	public Transacao cadastrarTransacao(@RequestBody @Valid Transacao transacao){
		
		
		return repositoryTransacao.save(transacao);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deletarTransacao(@PathVariable Integer id){
		
		Transacao transacao = repositoryTransacao.findOne(id);
		
		if(transacao == null){
		
			return ResponseEntity.notFound().build();
		}
		
		repositoryTransacao.delete(transacao);
		
		return ResponseEntity.noContent().build();

	}
}
