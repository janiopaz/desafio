package com.conductor.desafio.resources;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.conductor.desafio.models.Pessoa;
import com.conductor.desafio.repository.PessoaRepository;

@RestController
@RequestMapping("/pessoa")
public class PessoaResource {
	
	@Autowired
	private PessoaRepository repositoryPessoa;
	
	@GetMapping
	public @ResponseBody Iterable<Pessoa> listarPessoas(){
		
		Iterable<Pessoa> listaPessoas = repositoryPessoa.findAll();
		
		System.out.println(repositoryPessoa.findBycpf("10516660403"));
		
		return listaPessoas;
	}
	
	@GetMapping("/{id}")
	public 	ResponseEntity<Pessoa> buscar(@PathVariable Integer id){
		
		Pessoa pessoa = repositoryPessoa.findOne(id);
		
		if(pessoa == null){
		
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(pessoa);
	}
	
	@PutMapping("/{id}")
	public 	ResponseEntity<Pessoa> atualizar(@PathVariable Integer id,@RequestBody @Valid Pessoa pessoa){
		
		Pessoa existente = repositoryPessoa.findOne(id);
		
		if(existente == null){
		
			return ResponseEntity.notFound().build();
		}
		
		BeanUtils.copyProperties(pessoa, existente, "id");
		
		existente = repositoryPessoa.save(existente);
		
		return ResponseEntity.ok(existente);
	}
	
	@PostMapping()
	public Pessoa cadastrarPessoa(@RequestBody @Valid Pessoa pessoa){
		
		return repositoryPessoa.save(pessoa);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deletarPessoa(@PathVariable Integer id){
		
		Pessoa pessoa = repositoryPessoa.findOne(id);
		
		if(pessoa == null){
		
			return ResponseEntity.notFound().build();
		}
		
		repositoryPessoa.delete(pessoa);
		
		return ResponseEntity.noContent().build();

	} 

}
