package com.conductor.desafio.resources;

public class WrapperTransacao {
	
	private long valor;
	private Integer idConta;
	
	public WrapperTransacao() {}
	
	public long getValor() {return valor;}
	
	public void setValor(long valor) {this.valor = valor;}

	public Integer getIdConta() {return idConta;}

	public void setIdConta(Integer idConta) {this.idConta = idConta;}
	
	

}
