package com.conductor.desafio.resources;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.conductor.desafio.models.Conta;
import com.conductor.desafio.models.Transacao;
import com.conductor.desafio.repository.ContaRepository;
import com.conductor.desafio.repository.TransacaoRepository;

@RestController
@RequestMapping("/conta")
public class ContaResource {

	@Autowired
	private  ContaRepository repositoryConta;
	@Autowired
	private  TransacaoRepository repositoryTransacao;			
	
	@GetMapping
	public @ResponseBody Iterable<Conta> listarPessoas(){
		
		Iterable<Conta> listaPessoas = repositoryConta.findAll();
		
		return listaPessoas;
	}
	
	@GetMapping("/{id}")
	public 	ResponseEntity<Conta> buscar(@PathVariable Integer id){
		
		Conta conta = repositoryConta.findOne(id);
		
		if(conta == null){
		
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(conta);
	}
	
	@PutMapping("/{id}")
	public 	ResponseEntity<Conta> atualizar(@PathVariable Integer id,@RequestBody @Valid Conta conta){
		
		Conta existente = repositoryConta.findOne(id);
		
		if(existente == null){
		
			return ResponseEntity.notFound().build();
		
		}
		
		BeanUtils.copyProperties(conta,existente, "id");
		
		existente = repositoryConta.save(existente);
		
		return ResponseEntity.ok(existente);
	}
	
	@PostMapping()
	public Conta cadastrarConta(@RequestBody @Valid Conta conta){
		
		return repositoryConta.save(conta);
	
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deletarConta(@PathVariable Integer id){
		
		Conta conta = repositoryConta.findOne(id);
		
		if(conta == null){
		
			return ResponseEntity.notFound().build();
		}
		
		repositoryConta.delete(conta);
		
		return ResponseEntity.noContent().build();

	}
	
	@GetMapping("/extrato/{id}")
	public ResponseEntity<List<Transacao>> extrato(@PathVariable Integer id){
		
		Conta conta = repositoryConta.findOne(id);
		List<Transacao> extratos = repositoryTransacao.findByidConta_id(id);
		
		if((conta != null) && (conta.isFlagAtivo()) && (extratos!= null)){
			
			return ResponseEntity.status(HttpStatus.OK).body(extratos);
		
		}else
		
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}
	
	@GetMapping("/saldo/{id}")
	public ResponseEntity consultarSaldoDeUmaConta(@PathVariable Integer id){
		
		Conta conta = repositoryConta.findOne(id);
		
		if(conta!=null){
		
			return ResponseEntity.status(HttpStatus.OK).body(conta.getSaldo());
		
		}else
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}
	
	@PostMapping("/depositar/{id}")
	public ResponseEntity depositar (@PathVariable Integer id,@RequestBody WrapperTransacao wraper){

		Conta conta = repositoryConta.findOne(id);
		Date date = new Date();
		
		if((conta!=null) && (conta.isFlagAtivo())){
			
			Transacao transacao = new Transacao();
			transacao.setDataTransacao(date);
			transacao.setValor(wraper.getValor());
			transacao.setIdConta(conta);
			repositoryTransacao.save(transacao);
			
			conta.setSaldo(conta.getSaldo()+wraper.getValor());
			repositoryConta.save(conta);
			
			return ResponseEntity.status(HttpStatus.OK).body(conta.getSaldo());
		
		}else
		
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}
	@PostMapping("/saque/{id}")
	public ResponseEntity sacar (@PathVariable Integer id,@RequestBody  WrapperTransacao wraper){

		Conta conta = repositoryConta.findByid(id);
		Date date = new Date();

		
		if((conta!=null) && (conta.isFlagAtivo()) &&(wraper.getValor()<=conta.getLimiteSaqueDiario())){
			
			Transacao transacao = new Transacao();
			transacao.setDataTransacao(date);
			transacao.setValor(wraper.getValor());
			transacao.setIdConta(conta);
			repositoryTransacao.save(transacao);
			
			conta.setSaldo(conta.getSaldo()-wraper.getValor());
			repositoryConta.save(conta);
			
			return ResponseEntity.status(HttpStatus.OK).body(conta.getSaldo());
			
		}else if((wraper.getValor()>=conta.getLimiteSaqueDiario()) ||(!conta.isFlagAtivo()==false)){
			
			return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).build();
		
		}else
			
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}
	

	
	@PostMapping("/extratoPorPeriodo/{id}")
	public ResponseEntity <List<Transacao>> extratoPorPeriodo(@PathVariable Integer id, 
			@RequestBody @Valid WrapperExtratoPeriodo wraper){
		
		Conta conta = repositoryConta.findOne(id);
		List<Transacao> extratos = repositoryTransacao.extratoPeriodo(id,wraper.getData1(), wraper.getData2());
		
		if((conta != null) && (conta.isFlagAtivo()) && (extratos!= null)){
			
			return ResponseEntity.status(HttpStatus.OK).body(extratos);
		
		}else
		
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
	}

}
