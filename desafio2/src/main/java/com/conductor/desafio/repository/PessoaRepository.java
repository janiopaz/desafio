package com.conductor.desafio.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.conductor.desafio.models.Pessoa;



public interface PessoaRepository extends JpaRepository<Pessoa, Integer>{
	
	List<Pessoa> findBycpf(String cpf);

}
