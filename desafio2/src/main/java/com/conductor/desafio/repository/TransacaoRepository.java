package com.conductor.desafio.repository;



import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.conductor.desafio.models.Transacao;

public interface TransacaoRepository extends JpaRepository<Transacao, Integer>{
	
	List<Transacao> findByidConta_id(Integer id);
	
	@Query("SELECT t FROM Transacao t WHERE "+
	"t.idConta.id = :id AND "+
	"t.dataTransacao BETWEEN :data1 AND :data2")
	List<Transacao> extratoPeriodo(@Param("id") Integer id, @Param("data1") Date data1,@Param("data2") Date data2);


}
