package com.conductor.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.conductor.desafio.models.Conta;

public interface ContaRepository extends JpaRepository<Conta, Integer>{
		
		Conta findByid(Integer id);
}
