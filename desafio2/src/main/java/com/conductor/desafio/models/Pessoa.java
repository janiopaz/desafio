package com.conductor.desafio.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@SequenceGenerator(name="pessoa", sequenceName="pessoa_seq", allocationSize=1)
public class Pessoa {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="pessoa")
	public Integer id;
	
	private String nome;
	
	@Column(nullable=false, unique=true)
	private String cpf;
	
	@Temporal(TemporalType.DATE)
	private Date dataDeNascimento;

	public Pessoa(int id) {
		this.id = id;
	}
	public Pessoa() {}
	
	public Integer getId() {return id;}

	public void setId(int id) {this.id = id;}

	public String getNome() {return nome;}

	public void setNome(String nome) {this.nome = nome;}

	public String getcpf() {return cpf;}

	public void setcpf(String cpf) {this.cpf = cpf;}

	public Date getDataDeNascimento() {return dataDeNascimento;}

	public void setDataDeNascimento(Date dataDeNascimento) {this.dataDeNascimento = dataDeNascimento;}
	
	
	
	

}
