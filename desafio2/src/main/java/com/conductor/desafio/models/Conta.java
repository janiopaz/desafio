package com.conductor.desafio.models;


import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;



@Entity
@SequenceGenerator(name="conta", sequenceName="conta_seq", allocationSize=1)
public class Conta{
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="conta")
	private Integer id;
	
	@OneToOne(targetEntity=Pessoa.class)
	private Pessoa idPessoa;
	
	private float saldo;
	
	@OneToMany(mappedBy = "idConta", targetEntity = Transacao.class, fetch = FetchType.LAZY)
	private Collection<Transacao> transacoes;
	
	private float limiteSaqueDiario;
	
	private boolean flagAtivo;
	
	private long tipoConta;
	
	@Temporal(TemporalType.DATE)
	private Date dataCriacao;
	
	public Conta(int id) {
		this.id = id;
	}

	public Conta() {}

	public Integer getId() {return id;}

	public void setId(int id) {this.id = id;}

	public Pessoa getIdPessoa() {return idPessoa;}

	public void setIdPessoa(Pessoa idPessoa) {this.idPessoa = idPessoa;}

	public float getSaldo() {return saldo;}

	public void setSaldo(float saldo) {this.saldo = saldo;}

	public float getLimiteSaqueDiario() {return limiteSaqueDiario;}

	public void setLimiteSaqueDiario(float limiteSaqueDiario) {this.limiteSaqueDiario = limiteSaqueDiario;}

	public boolean isFlagAtivo() {return flagAtivo;}

	public void setFlagAtivo(boolean flagAtivo) {this.flagAtivo = flagAtivo;}

	public long getTipoConta() {return tipoConta;}

	public void setTipoConta(long tipoConta) {this.tipoConta = tipoConta;}

	public Date getDataCricao() {return dataCriacao;}

	public void setDataCricao(Date dataCricao) {this.dataCriacao = dataCricao;}
}
