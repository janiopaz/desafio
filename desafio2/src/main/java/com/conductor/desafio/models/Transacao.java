package com.conductor.desafio.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@SequenceGenerator(name="transacao", sequenceName="transacao_seq", allocationSize=1)
public class Transacao {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="transacao")
	public Integer id;
	
	@ManyToOne()
	public Conta idConta;
	
	private long valor;
	
	@Temporal(TemporalType.DATE)
	private Date dataTransacao;
	
	public Transacao(int id) {
		this.id = id;
		}
	public Transacao() {}

	public Integer getId() {return id;}

	public void setId(int id) {this.id = id;}

	public Conta getIdConta() {return idConta;}

	public void setIdConta(Conta idConta) {this.idConta = idConta;}

	public long getValor() {return valor;}

	public void setValor(long valor) {this.valor = valor;}

	public Date getDataTransacao() {return dataTransacao;}

	public void setDataTransacao(Date dataTransacao) {this.dataTransacao = dataTransacao;}
}
